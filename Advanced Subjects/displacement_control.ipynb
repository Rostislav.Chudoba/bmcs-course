{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Displacement control"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider a set of $O$ nonlinear equations with unknown parameters $U_O$ and load vector $P_O$ \n",
    "\\begin{align}\n",
    "R_O(U_O) :=  F_O( U_O ) - P_O = 0\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assume that a set of variables $I \\in O$ is explicitly prescribed so that only $J \\in O - I$ are unknown. Then, let us separate the equations associated with these unknowns and those associated with the remaining variables\n",
    "\\begin{align}\n",
    " R_O := F_O( \\bar{U}_{O_I}, U_{O_J} ) - P_O & = 0 \\\\\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{align}\n",
    "R_O(\\bar{U}_I, U_J ) = R_O(\\bar{U}_I, U_J^k ) + \n",
    "\\left.\n",
    "\\frac{\\partial F_O(\\bar{U}_I, U_J)}{\\partial U_P}\n",
    "\\right|_k\n",
    "\\Delta U_P^{(k+1)} = 0\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{align}\n",
    "R_O(\\bar{U}_I, U_J ) &= R_O(\\bar{U}_I, U_J^k ) \\\\\n",
    "&+  \n",
    "\\left.\n",
    "\\frac{\\partial F_O(\\bar{U}_I, U_J)}{\\partial \\bar{U}_I}\n",
    "\\right|_k\n",
    "\\Delta \\bar{U}_I + \n",
    "\\left.\n",
    "\\frac{\\partial F_O(\\bar{U}_I, U_J)}{\\partial U_J}\n",
    "\\right|_k\n",
    "\\Delta U_J^{(k+1)} = 0\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us provide the mapping $O_I := I \\rightarrow O$ and $J \\rightarrow O$. Then, we can write the force vector as\n",
    "\\begin{align}\n",
    "F_O = F_{O_I} + F_{O_J}\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "F_O = np.arange(5)\n",
    "O_I = np.array([1])\n",
    "O_J = np.array([0,2,3,4])\n",
    "F_I = F_O[O_I]\n",
    "F_J = F_O[O_J]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{align}\n",
    "R_O(\\bar{U}_I, U_J ) &= R_O(\\bar{U}_I, U_J^k ) \n",
    "\\\\\n",
    "&+  \n",
    "\\left.\n",
    "\\frac{\\partial F_I(\\bar{U}_I, U_J)}{\\partial \\bar{U}_I}\n",
    "\\right|_k\n",
    "\\Delta \\bar{U}_I + \n",
    "\\left.\n",
    "\\frac{\\partial F_I(\\bar{U}_I, U_J)}{\\partial U_J}\n",
    "\\right|_k\n",
    "\\Delta U_J^{(k+1)} \n",
    "\\\\\n",
    "&+  \n",
    "\\left.\n",
    "\\frac{\\partial F_J(\\bar{U}_I, U_J)}{\\partial \\bar{U}_I}\n",
    "\\right|_k\n",
    "\\Delta \\bar{U}_I +\n",
    "\\left.\n",
    "\\frac{\\partial F_J(\\bar{U}_I, U_J)}{\\partial U_J}\n",
    "\\right|_k\n",
    "\\Delta U_J^{(k+1)} = 0\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By resolving for $\\Delta U_J^{(k+1)}$ we obtain\n",
    "\\begin{align}\n",
    "\\left.\n",
    "\\frac{\\partial F_J(\\bar{U}_I, U_J)}{\\partial U_J}\n",
    "\\right|_k\n",
    "\\Delta U_J^{(k+1)} = -\n",
    "R_J(\\bar{U}_I, U_J^k ) - \n",
    "\\left.\n",
    "\\frac{\\partial F_J(\\bar{U}_I, U_J)}{\\partial \\bar{U}_I}\n",
    "\\right|_k\n",
    "\\Delta \\bar{U}_I\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And by substituting for the residuum\n",
    "\\begin{align}\n",
    "\\left.\n",
    "\\frac{\\partial F_J(\\bar{U}_I, U_J)}{\\partial U_K}\n",
    "\\right|_k\n",
    "\\Delta U_K^{(k+1)} = \\bar{P}_J -\n",
    "F_J(\\bar{U}_I, U_J^k )\n",
    "-\n",
    "\\left.\n",
    "\\frac{\\partial F_J(\\bar{U}_I, U_J)}{\\partial \\bar{U}_I}\n",
    "\\right|_k\n",
    "\\Delta \\bar{U}_I\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To introduce the boundary conditions\n",
    "$u_\\mathrm{m}(x=0) = 0$ and $u_\\mathrm{f}(x=0) = w$\n",
    "we need to rearrange the system of nonlinear equilibrium conditions\n",
    "into two parts:\n",
    "\\begin{align}\n",
    " R_{0} ( \\bar{U}_{0}, U_{1}) &= 0, \\\\\n",
    " R_{1} ( \\bar{U}_{0}, U_{1}) &= P\n",
    "\\end{align}\n",
    "where $I_1 = 1 \\ldots M$ counts the non-constrained nodes located\n",
    "within $\\Omega-\\Gamma_{u_c}$, i.e $-L \\leq x < 0$.\n",
    "The equations indexed $0$\n",
    "represent the reaction forces \n",
    "at the constrained boundary $x=0$. \n",
    "The iterative scheme for\n",
    "the direct pull-out displacement control is obtained by \n",
    "performing the linearization of the unconstrained residuum equations and rewriting the iterative \n",
    "scheme as\n",
    "\\begin{equation} \n",
    "K_{I1}^{(\\theta,k-1)} \\Delta U_{I1}^{(k)} = \n",
    "- R^{(\\theta)}_{cI_1}(d_{dJ_1}^{(k-1)}) \n",
    "- K_{cI_1d0}^{(\\theta,k-1)} \\Delta \\bar{d}_{c0}^{(k)}.\n",
    "\\end{equation}\n",
    "  This equation is solved iteratively until the convergence is reached. With the obtained nodal displacement vector $d_{cI_1}$, the pullout force\n",
    "can be evaluated by substituting it back to Eq.~(\\ref{eq:displ_constrined_residuum})\n",
    "\\begin{align}\n",
    "\\label{eq:finite_element_pull_out}\n",
    "P = R_{\\mathrm{f}0}( \\bar{d}_{\\mathrm{m}0} =0, \\bar{d}_{\\mathrm{f}0} = w, d_{cI_1}).\n",
    "\\end{align}\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
