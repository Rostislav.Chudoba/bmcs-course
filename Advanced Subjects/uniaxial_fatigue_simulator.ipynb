{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simulation of fatigue for uniaxial stress state"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assume a uniaxial stress state with $\\sigma_{11} = \\bar{\\sigma}(\\theta)$ representing the loading function. All other components of the stress tensor are assumed zero \n",
    "\\begin{align}\n",
    "\\sigma_{ab} = 0; \\forall a,b \\in (0,1,2), a = b \\neq 1\n",
    "\\end{align}\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "from mathkit.matrix_la.sys_mtx_assembly import SysMtxAssembly\n",
    "import numpy as np\n",
    "import sympy as sp\n",
    "sp.init_printing()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Assembly operators\n",
    "To construct the mapping between the tensorial representation of the stress and states and the equilibrium equations used to solve to find the displacement for a given load increment let us introduce the tensor mapping operators:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Kronecker delta**: defined as \n",
    "\\begin{align}\n",
    "\\delta_{ab} = 1 \\;  \\mathrm{for} \\; a = b \\;  \\mathrm{and} \\; \\; \\delta_{ab} = 0 \\; \\mathrm{for} \\; a \\neq 0\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "DELTA = np.identity(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Levi-Civita operator**: defined as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Levi Civita symbol\n",
    "EPS = np.zeros((3, 3, 3), dtype='f')\n",
    "EPS[(0, 1, 2), (1, 2, 0), (2, 0, 1)] = 1\n",
    "EPS[(2, 1, 0), (1, 0, 2), (0, 2, 1)] = -1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, consider a constitutive relation given in tensorial form as \n",
    "\\begin{align}\n",
    "\\sigma_{ab} = D_{abcd} \\varepsilon_{cd} \n",
    "\\end{align}\n",
    "Since $\\sigma_{ab} = \\sigma_{ba}$ we can write as well\n",
    "\\begin{align}\n",
    "\\sigma_{ba} = D_{bacd} \\varepsilon_{cd} \n",
    "\\end{align}\n",
    "and since $\\varepsilon_{cd} = \\varepsilon_{dc}$\n",
    "\\begin{align}\n",
    "\\sigma_{ab} = D_{abdc} \\varepsilon_{dc} \n",
    "\\end{align}\n",
    "\\begin{align}\n",
    "\\sigma_{ba} = D_{badc} \\varepsilon_{dc} \n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given an engineering strain and stress vectors as \n",
    "$\\sigma^{\\mathrm{eng}}_i$\n",
    "we define write the mapping \n",
    "\\begin{align}\n",
    "\\sigma^{\\mathrm{eng}}_a = \\sigma_{aa} \n",
    "\\end{align}\n",
    "and\n",
    "\\begin{align}\n",
    "\\sigma^{\\mathrm{eng}}_{3+a} = \\sigma_{aa} \n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{align}\n",
    "\\sigma_O = D_{OP} \\varepsilon_O\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "DD = np.hstack([DELTA, np.zeros_like(DELTA)])\n",
    "EEPS = np.hstack([np.zeros_like(EPS), EPS])\n",
    "\n",
    "GAMMA = np.einsum(\n",
    "    'ik,jk->kij', DD, DD\n",
    ") + np.einsum(\n",
    "    'ikj->kij', np.fabs(EEPS)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{align}\n",
    "   \\varepsilon^{\\mathrm{eng}}_k = \\Gamma_{kab} \\; \\varepsilon_{ab}\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_eps_ab = lambda eps_O: np.einsum(\n",
    "    'Oab,...O->...ab', GAMMA, eps_O\n",
    ")[np.newaxis, ...]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "GAMMA_inv = np.einsum(\n",
    "    'aO,bO->Oab', DD, DD\n",
    ") + 0.5 * np.einsum(\n",
    "    'aOb->Oab', np.fabs(EEPS)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{align}\n",
    "   \\sigma^{\\mathrm{eng}}_k = \\Gamma^{-1}_{kab} \\sigma_{ab}\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_sig_O = lambda sig_ab: np.einsum(\n",
    "    'Oab,...ab->...O', GAMMA_inv, sig_ab\n",
    ")[0,...]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "GG = np.einsum(\n",
    "    'Oab,Pcd->OPabcd', GAMMA_inv, GAMMA_inv\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_K_OP = lambda D_abcd: np.einsum(\n",
    "    'OPabcd,abcd->OP', GG, D_abcd\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Material model\n",
    "Let us define an elastic material model as a reference"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "mu = 1; lambda_ = 1\n",
    "get_sig_ab = lambda eps_cd: (\n",
    "    2 * mu * eps_cd + lambda_ * np.einsum(\n",
    "        'ab,kk->ab', DELTA, eps_cd\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ibvpy.mats.mats3D.mats3D_plastic.vmats3D_desmorat import \\\n",
    "    MATS3DDesmorat\n",
    "m = MATS3DDesmorat(K=0, gamma=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ibvpy.mats.mats3D.mats3D_sdamage.vmats3D_sdamage import \\\n",
    "    MATS3DScalarDamage\n",
    "m = MATS3DScalarDamage()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "import copy\n",
    "def get_UF_t(F, n_t):\n",
    "    state_vars = {\n",
    "        name: np.zeros(shape, dtype=np.float_)[np.newaxis,...]\n",
    "        for name, shape in m.state_var_shapes.items() \n",
    "    }\n",
    "    # total number of DOFs\n",
    "    n_O = 6\n",
    "    # Global vectors\n",
    "    F_ext = np.zeros((n_O,), np.float_)\n",
    "    F_O = np.zeros((n_O,), np.float_)\n",
    "    U_k_O = np.zeros((n_O,), dtype=np.float_)\n",
    "    # Setup the system matrix with displacement constraints\n",
    "    # Time stepping parameters\n",
    "    t_n1, t_max, t_step = 0, 1, 1/n_t\n",
    "    # Iteration parameters\n",
    "    k_max, R_acc = 1000, 1e-3\n",
    "    # Record solutions\n",
    "    U_t_list, F_t_list = [np.copy(U_k_O)], [np.copy(F_O)]\n",
    "    state_var_list = [copy.deepcopy(state_vars)]\n",
    "    # Load increment loop\n",
    "    while t_n1 <= t_max:\n",
    "        F_ext[0] = F(t_n1)\n",
    "        k = 0\n",
    "        # Equilibrium iteration loop\n",
    "        while k < k_max:\n",
    "            print('iteration', k)\n",
    "            # Transform the primary vector to field\n",
    "            eps_ab = get_eps_ab(U_k_O)\n",
    "            # Stress and material stiffness\n",
    "            sig_ab, D_abcd = m.get_corr_pred(\n",
    "                eps_ab,1,**state_vars\n",
    "            )\n",
    "            # Internal force\n",
    "            F_O = get_sig_O(sig_ab)\n",
    "            # Residuum\n",
    "            R_O = F_ext - F_O\n",
    "            # System matrix\n",
    "            K_OP = get_K_OP(D_abcd[0])\n",
    "            # Convergence criterion\n",
    "            R_norm = np.linalg.norm(R_O)\n",
    "            if R_norm < R_acc:\n",
    "                # Convergence reached\n",
    "                break \n",
    "            # Next iteration\n",
    "            delta_U_O = np.linalg.solve(K_OP, R_O)\n",
    "            U_k_O += delta_U_O\n",
    "            k += 1\n",
    "        else:\n",
    "            print('no convergence')\n",
    "            break\n",
    "\n",
    "        # Target time of the next load increment\n",
    "        U_t_list.append(np.copy(U_k_O))\n",
    "        F_t_list.append(F_O)\n",
    "        state_var_list.append(copy.deepcopy(state_vars))\n",
    "        t_n1 += t_step\n",
    "\n",
    "    U_t, F_t = np.array(U_t_list), np.array(F_t_list)\n",
    "    return U_t, F_t, state_var_list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "iteration 0\n",
      "iteration 0\n",
      "iteration 1\n",
      "iteration 0\n",
      "iteration 1\n",
      "iteration 0\n",
      "iteration 1\n",
      "iteration 0\n",
      "iteration 1\n",
      "iteration 0\n",
      "iteration 1\n",
      "iteration 0\n",
      "iteration 1\n",
      "iteration 0\n",
      "iteration 1\n",
      "iteration 0\n",
      "iteration 1\n",
      "iteration 0\n",
      "iteration 1\n",
      "iteration 0\n",
      "iteration 1\n"
     ]
    }
   ],
   "source": [
    "U, F, S = get_UF_t(\n",
    "    F=lambda t: -170 * t, \n",
    "    n_t=10\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Examples of postprocessing**:\n",
    "Plot the axial strain against the lateral strain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAVAAAAD4CAYAAAC60L7uAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjAsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+17YcXAAAgAElEQVR4nO3dd3hUZfrG8e8zCQnVAEnoLUgEAkgbUpSiIFIsseAKoqKCrAoqUlZc19WfbmMBQRRELKyoCIgFVAQRRMqmkFANEAhNQg0CkRogeX9/5OxujClDksmZmTyf6+Ji5pz3vHO/Bu9rMuckR4wxKKWUunIOuwMopZS30gJVSqkS0gJVSqkS0gJVSqkS0gJVSqkS8rc7QFkICQkxzZo1szuGUsrHJCcnHzfGhBa23ycKtFmzZiQlJdkdQynlY0Rkf1H79Vt4pZQqIS1QpZQqIS1QpZQqIS1QpZQqIS1QpZQqIZcKVET6ikiqiKSJyPgC9geKyHxrf4KINMuz7zlre6qI9Mmz/T0ROSYiP+abq7aILBeRXdbftUq+PKWUcp9iC1RE/IDpQD8gAhgkIhH5hg0FThpjWgBTgAnWsRHAQKAN0BeYYc0H8C9rW37jgRXGmHBghfVcKaU8jivvQCOBNGPMHmPMRWAeEJtvTCzwvvV4IdBLRMTaPs8Yk2WM2QukWfNhjFkNnCjg9fLO9T5wxxWsp1jnzmSSMH0omT8fLctplVIVkCsF2hA4kOd5urWtwDHGmMtAJhDs4rH51TXGHLbmOgzUKWiQiAwXkSQRScrIyHBhGbl2b1hJx2Ofc/aNbqRtXufycUoplZ8rBSoFbMv/W5gLG+PKsSVijJlljHEaY5yhoYX+pNVvtOt+J3tu+wR/c4lGn8WS+PnrZRFHKVUBuVKg6UDjPM8bAYcKGyMi/kAQud+eu3JsfkdFpL41V33gmAsZr0grZy/8Hl9DWuU2RG7+EwnTHiDrwrmyfhmllI9zpUDXA+EiEiYiAeSeFFqcb8xiYIj1eACw0uTeK2QxMNA6Sx8GhAOJxbxe3rmGAItcyHjFgus2otXY5cQ1eJCoE4vZP6kHR37a5Y6XUkr5qGIL1PpMcySwDNgOLDDGpIjIyyJyuzXsXSBYRNKA0Vhnzo0xKcACYBuwFBhhjMkGEJGPgTigpYiki8hQa65/AL1FZBfQ23ruFv6VAogZ/jobr5tO/UsHCHzvRrau/txdL6eU8jHiCzeVczqdprS/jenArs1kf3w/TbIPkBj2BJEPvILDz6/4A5VSPktEko0xzsL2608iWRqHt6fO6LVsuKon0fums2XyLWSePG53LKWUB9MCzaNq9SA6P7OQ+JbP0uZsIqenXc+eHxPsjqWU8lBaoPmIw0H0oD+y+5b5BJiL1P/kVtYvmmF3LKWUB9ICLUSryN44HlvNnsCWdNn4HAlvPMzFrAt2x1JKeRAt0CKE1GtMy3Eria83mKjjn7F3YneOpu+2O5ZSykNogRbDv1IA0Y/NYEPUVBpd2k+ld27gx7X5L4NVSlVEWqAu6tTvYY7ft4zTjiBaL3+QuDkvYHJy7I6llLKRFugVaNqyAyHPrGVTjR7E7JnGpkm38supn+2OpZSyiRboFapWoyadRn9O/DVjaXc2jszXurJ323q7YymlbKAFWgLicBB93wvs7PcxVcw56s6/haQv37I7llKqnGmBlkJEdF8Yvpr9AS1wJv+BhOlD9VInpSoQLdBSCmnQlBbjvie+7kCiMhayZ9KNHDu41+5YSqlyoAVaBioFBBL9+FskR75Kk4u78Xu7BynrvrY7llLKzbRAy1Dn/kPJGPgNZx3Vafnt/cR/+KJe6qSUD9MCLWNNW3em1tNr2VK9K9FpU9k4OZYzv5y0O5ZSyg20QN2gRlBtOo5ZRHyLUVx7Zi0npl7P/u3JdsdSSpUxLVA3EYeD6Pv/j9SbP6RazhlC5/Ujecm7dsdSSpUhLVA3a3P9LWQ/+gM/BVxN58TRxL/5ey5dzLI7llKqDGiBloM6DcNoPvZ7EkIHEH10Hrsm9uT4of12x1JKlZIWaDkJCKxM1Ih3Ser8T5pd3AWzurM9YZndsZRSpaAFWs6ct/2eo/d+zXmpSoslg4if+4pe6qSUl9ICtUFYRBeCnl7Lj9WiiN45iQ1T7uLs6VN2x1JKXSEtUJtcVTOY9mO+Ii5sJB1+WUXGlK7sT91kdyyl1BXQArWRw8+PmCF/ZftN7xOUk0nI3D5sWPovu2MppVykBeoB2naLJWvo96RXakqn+KeJn/kEly9dtDuWUqoYWqAeol7jFjQbu4qEkLuIPvIRqRN7cfzIAbtjKaWKoAXqQQIrVyVq5GzWd/w7zbN2kDOzOzvWf2d3LKVUIbRAPVCX2Cc4NOBLLkklmn/1OxLm/V0vdVLKA2mBeqir20VT/cl1bKvahagd/yB56j2cO5NpdyylVB5aoB4sqHYo145dQlzTx+iUuYKjr3bjQNpWu2MppSxaoB7O4edHzMMTSOn5HrVyfqbmB73Z+O2HdsdSSuFigYpIXxFJFZE0ERlfwP5AEZlv7U8QkWZ59j1nbU8VkT7FzSkivURkg4hsEpG1ItKidEv0De163MX5h1dypFIjOv57BHGzniL78mW7YylVoRVboCLiB0wH+gERwCARicg3bChw0hjTApgCTLCOjQAGAm2AvsAMEfErZs43gcHGmA7AXOBPpVui76jftCWNx/xAQu3biTn0Ptsn9uLEsYN2x1KqwnLlHWgkkGaM2WOMuQjMA2LzjYkF3rceLwR6iYhY2+cZY7KMMXuBNGu+ouY0wFXW4yDgUMmW5psqV6lG1FMfkNj+FcIvpHBpRjdSk1baHUupCsmVAm0I5L2iO93aVuAYY8xlIBMILuLYouYcBiwRkXTgAeAfBYUSkeEikiQiSRkZGS4sw7dE3vkUB+76gmzxI+zLASQsmKiXOilVzlwpUClgm3FxzJVuB3gG6G+MaQTMBl4tKJQxZpYxxmmMcYaGhhYY3Ne1aN+VaiPXsr1KJ6K2/YWk1wZy/uxpu2MpVWG4UqDpQOM8zxvx22+r/ztGRPzJ/db7RBHHFrhdREKB9saYBGv7fOA6l1ZSQQUF16XduGXENRlO51PfcujVbhzck2J3LKUqBFcKdD0QLiJhIhJA7kmhxfnGLAaGWI8HACuNMcbaPtA6Sx8GhAOJRcx5EggSkWusuXoD20u+vIrB4edHzCMT2XrD24RkH6PGnJvYtGKe3bGU8nn+xQ0wxlwWkZHAMsAPeM8YkyIiLwNJxpjFwLvAByKSRu47z4HWsSkisgDYBlwGRhhjsgEKmtPa/ijwqYjkkFuoj5Tpin1Y+xvv4VCzdpz78D46rPk9cXsTiHxoIn7+xX6ZlVIlILlvFL2b0+k0SUlJdsfwGBfOnWHLrEeJPLWELZU702TYXGqG1LM7llJeR0SSjTHOwvbrTyL5oMpVqxM56mMS271Eq/ObufBGV3ZtXG13LKV8jhaoD4u8+xn23/E5AE2/uJPET6fYnEgp36IF6uPCO3YncMQadlRpT+TWl0icOogL587YHUspn6AFWgHUCq1Pm7HfEtfoESJPLSF9cncO7d1hdyylvJ4WaAXh5+9PzLApbOr2FnWyD1Pt/Z5s/v4Tu2Mp5dW0QCuYDr0GcvrB7zjuV4d2qx4l7r1x5GRn2x1LKa+kBVoBNWzehgaj15Bc82ZifprF1ol9yPz5qN2xlPI6WqAVVJVqNXA+PY+EiD/R+vwGzr7RlbTN6+yOpZRX0QKtwMThIOp349h720L8TDaNP4sl8fNpdsdSymtogSpaOntS6Yk17KrchsjNL5A47X4unD9rdyylPJ4WqAKgdp2GtB63grgGQ4g88SUHJvfg8P5Uu2Mp5dG0QNV/+fn7EzN8Ghuvm069S+lUmd2TrT98ZncspTyWFqj6jY4338+pB5Zz0hFMm5WPEDf7Wb3USakCaIGqAjVu0Y66o9ewIagXMftnsmVSfzJPHrc7llIeRQtUFapq9SA6j/qEhFbjaXNuPWemXcfurfF2x1LKY2iBqiKJw0HUwOfYfesCKplLNFx4K+u/mG53LKU8ghaockmrLjfheGw1uwNb02XTH0l4fQhZF87ZHUspW2mBKpeF1GtMy3EriK83mKifv2D/pB4cOZBmdyylbKMFqq6If6UAoh+bwYbo12hw6QCB797Ij2sW2R1LKVtogaoS6dT3IU4MXkqmI4jW3w0h7v3nMTk5dsdSqlxpgaoSa3JNB0KfWcumq24gZu8bbJp0K7+c+tnuWEqVGy1QVSrVatSk0zOfEX/NONqejSfzta7sTUmwO5ZS5UILVJWaOBxE3/cn0vp/TBVzjroLbiPpy7fsjqWU22mBqjLTOqoPDF/NvoBwnMl/IGH6UC5mXbA7llJuowWqylRIg6aEj1tJfN1BRGUsZM/EGzh2cK/dsZRyCy1QVeYqBQQS/fhMkiNfpcmlPfi93YOUdV/bHUupMqcFqtymc/+hZAxayhlHDVp+ez/xH76olzopn6IFqtyqaatOBI9ay5YaXYlOm8rGybGc+eWk3bGUKhNaoMrtql9Vi46jFxEfPpprz6zlxNTr2b892e5YSpWaFqgqF+JwED34RVL7fETVnLOEzutH8pJ37Y6lVKm4VKAi0ldEUkUkTUTGF7A/UETmW/sTRKRZnn3PWdtTRaRPcXNKrr+KyE4R2S4iT5VuicqTtLmuP2b4D/wUcDWdE0cTP2M4ly5m2R1LqRIptkBFxA+YDvQDIoBBIhKRb9hQ4KQxpgUwBZhgHRsBDATaAH2BGSLiV8ycDwGNgVbGmNbAvFKtUHmc0AbNaD72e+JD7yH62HzSJt7I8UP77Y6l1BVz5R1oJJBmjNljjLlIbqHF5hsTC7xvPV4I9BIRsbbPM8ZkGWP2AmnWfEXN+TjwsjEmB8AYc6zky1OeKiCwMtEj3iHJOZGmF9NgVne2xS+1O5ZSV8SVAm0IHMjzPN3aVuAYY8xlIBMILuLYoua8GrhXRJJE5BsRCS8olIgMt8YkZWRkuLAM5Ymctw7n6L1fc16qcs03g4if+4pe6qS8hisFKgVsMy6OudLtAIHABWOME3gbeK+gUMaYWcYYpzHGGRoaWmBw5R3CIrpQc9Q6tla/juidk9gw5S7Onj5ldyyliuVKgaaT+5nkfzQCDhU2RkT8gSDgRBHHFjVnOvCp9fhz4FoXMiovVyOoNh3GfEl886fo8Msqjk/pyv7UTXbHUqpIrhToeiBcRMJEJIDck0KL841ZDAyxHg8AVhpjjLV9oHWWPgwIBxKLmfMLoKf1uAews2RLU95GHA6iH3yF7b3nUCMnk5C5fdiw9F92x1KqUMUWqPWZ5khgGbAdWGCMSRGRl0XkdmvYu0CwiKQBo4Hx1rEpwAJgG7AUGGGMyS5sTmuufwB3i8hW4O/AsLJZqvIWbbvezqVhq0iv1JRO8U8TP/MJLl+6aHcspX5Dct8oejen02mSkpLsjqHKWNaFc2x6ZwRRxz8jJeBa6j4yl5B6jYs/UKkyIiLJ1vmYAulPIimPFVi5KlEjZ7O+499pnrWDnJnd2ZG43O5YSv2XFqjyeF1in+DwPV9xUQK4+ut7if/4b3qpk/IIWqDKKzRvG0WNp9aRUi2S6NQJJE+9h3NnMu2OpSo4LVDlNYJqhXDtmK+JbzaCTpkrOPpqNw7s2mx3LFWBaYEqr+Lw8yP6ob+R0ms2NXNOUPPDPmz89kO7Y6kKSgtUeaV23e8k65HvOVKpER3/PYK4t57US51UudMCVV6rXpNwmoxdTUJwLDGH57BjUm9+PppudyxVgWiBKq8WWLkqUU/OIbH9X2hxIYXLb3YnNWml3bFUBaEFqnxC5J1Pkn7XIrLFn7AvB5Cw4J96qZNyOy1Q5TNatL+eak+uY3vVzkRt+ytJr93L+bOn7Y6lfJgWqPIpQbVDaTd2KXFNfk/nU8s5PLkrB/ekFH+gUiWgBap8jsPPj5hH/smPN7xDcE4GNebcxKbvPrY7lvJBWqDKZ1174wDODlnJMb/6dFj7GHFvjyL78mW7YykfogWqfFqDsFY0GruGxFq3EHNwNtsm9uZkxmG7YykfoQWqfF7lKtWIfHouie1e4poLW8ma3o2dG36wO5byAVqgqsKIvPsZfrrjMwzQbNFdJHwyWS91UqWiBaoqlPCO3akyci07qnQgKuVl1k8bzIVzZ+yOpbyUFqiqcGqG1KPN2GXENxpK5KklpE/uzsE92+2OpbyQFqiqkPz8/Yke9iqbur1FnewjVJ/Ti80rF9gdS3kZLVBVoXXoNZDTD67guF8d2v0wnLh3x+ilTsplWqCqwmvYvDUNRq8huWYfYg68Q8qkvmT+fNTuWMoLaIEqBVSpVgPn0x+T0OYFWp3fyNk3upK2ea3dsZSH0wJVyiIOB1H3jGVf7Kc4TA6NP7uD9Z+9Zncs5cG0QJXK55pONxDwxGp2Vm5Lly1/JvG1wVw4f9buWMoDaYEqVYDadRoSMe474ho+ROTJrzgwqTuH96faHUt5GC1QpQrh5+9PzKOvsfG66dS7fJAqs3uyZdWndsdSHkQLVKlidLz5fk49sJyTjhDafj+UuNnPkpOdbXcs5QG0QJVyQeMW7ag7ejUbgm4iZv9Mtk7qR+aJDLtjKZtpgSrloqrVg+g8agEJrZ+j9bkkzrx+Pbu3/NvuWMpGWqBKXQFxOIi6dzx7bl1AJXOJhp/ezvov3rA7lrKJFqhSJdCqy034Pb6G3YERdNn0PAmvDyHrwjm7Y6ly5lKBikhfEUkVkTQRGV/A/kARmW/tTxCRZnn2PWdtTxWRPlcw5+sior9nTHms4LqNaDnuO+Lq30/Uz1+wf1IPjvy0y+5YqhwVW6Ai4gdMB/oBEcAgEYnIN2wocNIY0wKYAkywjo0ABgJtgL7ADBHxK25OEXECNUu5NqXczr9SADG/n87GmGk0uHSAwPd6snX1IrtjqXLiyjvQSCDNGLPHGHMRmAfE5hsTC7xvPV4I9BIRsbbPM8ZkGWP2AmnWfIXOaZXrROAPpVuaUuWnY58hnBi8lExHTSJWDCH+X3/US50qAFcKtCFwIM/zdGtbgWOMMZeBTCC4iGOLmnMksNgYU+Sdv0RkuIgkiUhSRoZeTqLs1+SaDoQ+s4ZNV91I9L7pbJ58K5knj9sdS7mRKwUqBWwzLo65ou0i0gC4B3i9uFDGmFnGGKcxxhkaGlrccKXKRbUaNen0zKfEt/wDbc8mcHpaV/amJNgdS7mJKwWaDjTO87wRcKiwMSLiDwQBJ4o4trDtHYEWQJqI7AOqikiai2tRyiOIw0H0oOfZ3X8egeYC9RbcStLimXbHUm7gSoGuB8JFJExEAsg9KbQ435jFwBDr8QBgpTHGWNsHWmfpw4BwILGwOY0xXxtj6hljmhljmgHnrBNTSnmdVlE3I4+tZm9gS5wbniXhjUe4mHXB7liqDBVboNZnmiOBZcB2YIExJkVEXhaR261h7wLB1rvF0cB469gUYAGwDVgKjDDGZBc2Z9kuTSn7hdRrQvjYFcTXHUTU8U/ZM/EGjqbvtjuWKiOS+0bRuzmdTpOUlGR3DKWKlLxkNq0TnuW8VOZw7+m0vf42uyOpYohIsjHGWdh+/UkkpcpJ5/4PkzFoKWccNWj97QPEz3kBk5NjdyxVClqgSpWjpq06ETxqLZtrdCN6zzQ2Tr6d05kn7I6lSkgLVKlyVv2qWnQcvYj48NFce2Ydp6Zez77t+hGUN9ICVcoG4nAQPfhFdvadSxVzjjrz+pP89Tt2x1JXSAtUKRtFxPTDDP+B/QFX03n9GOJnDOfSxSy7YykXaYEqZbPQBs1oMW4V8XV+R/Sx+aRNvJHjh/bbHUu5QAtUKQ9QKSCQ6CfeJsk5kaYX02BWd7bFfWN3LFUMLVClPIjz1uEcvfdrzktVrll6H/Ef/Z9e6uTBtECV8jBhEV2oOWodW6tfR/SuV9n46h2c+eWk3bFUAbRAlfJANYJq02HMl8Q3f4r2p1fz89Ru7E/dZHcslY8WqFIeShwOoh98he2951AjJ5OQuX3Y8M1su2OpPLRAlfJwbbvezqVhq0iv1IxOCaOIf/MxLl+6aHcshRaoUl6hbqOrCRv3AwkhdxF99GNSJ/bk+JGf7I5V4WmBKuUlAgIrEzVyNkmd/kHzrFTMzO7sSPjW7lgVmhaoUl7GefvjHL7nK7IkkKuXDCT+47/qpU420QJVygs1bxtFjafWkVItkujUf5I8ZQDnzmTaHavC0QJVyksF1Qrh2jFfE99sBB1/WcmxV7tyYNdmu2NVKFqgSnkxh58f0Q/9jW29ZhOUc5JaH/Zhw7IP7I5VYWiBKuUD2nW/k6xHvudQpcZ0ihtJ3FtP6qVO5UALVCkfUa9JOE3H/kBCcCwxh+eQOvEmfj6abncsn6YFqpQPCaxclagn55DY/i9cnbWN7De7sSNphd2xfJYWqFI+KPLOJzl492IuSyWaf3kPCfMn6KVObqAFqpSPuvra66j25Dq2V+1M1Pa/kTz1d5w/e9ruWD5FC1QpHxZUO5R2Y5cS1/QxOmV+x+HJXUlP+9HuWD5DC1QpH+fw8yPm4Qn8eMM71M45zlUf9mbT8rl2x/IJWqBKVRDX3jiAcw+t5JhffTqse5y4t0eRffmy3bG8mhaoUhVIg2YtaTR2DYm1biHm4Gy2TezNyYzDdsfyWlqgSlUwlatUI/LpuSS2e4lrLmwla3pXdm5YZXcsr6QFqlQFFXn3M/x05+cYhGaL7ibhk0l6qdMV0gJVqgIL79CNKiPXsqNKB6JSXiFp2n1cOHfG7lhew6UCFZG+IpIqImkiMr6A/YEiMt/anyAizfLse87anioifYqbU0Q+srb/KCLviUil0i1RKVWUmiH1aDN2GXGNh9Hl1DccnNyNg3u22x3LKxRboCLiB0wH+gERwCARicg3bChw0hjTApgCTLCOjQAGAm2AvsAMEfErZs6PgFZAO6AKMKxUK1RKFcvP35+YoZPZ3P0tQrOPUmNOLzavXGB3LI/nyjvQSCDNGLPHGHMRmAfE5hsTC7xvPV4I9BIRsbbPM8ZkGWP2AmnWfIXOaYxZYixAItCodEtUSrmqfc+BnH5wBRl+dWm/+lHi3h2jlzoVwZUCbQgcyPM83dpW4BhjzGUgEwgu4thi57S+dX8AWFpQKBEZLiJJIpKUkZHhwjKUUq5o2Lw1DcesYX3NfsQceIeUSX04dfyI3bE8kisFKgVsMy6OudLtec0AVhtj1hQUyhgzyxjjNMY4Q0NDCxqilCqhylWr43xqLgltXqDV+U2cf6MruzYV+L9iheZKgaYDjfM8bwQcKmyMiPgDQcCJIo4tck4ReREIBUa7sgilVNkTh4Ooe8ayL/YzBEOTz+8k8dOpdsfyKK4U6HogXETCRCSA3JNCi/ONWQwMsR4PAFZan2EuBgZaZ+nDgHByP9csdE4RGQb0AQYZY/SiNKVsdk2nHgSOWMvOym2J3Poiia/dx4XzZ+2O5RGKLVDrM82RwDJgO7DAGJMiIi+LyO3WsHeBYBFJI/dd43jr2BRgAbCN3M8yRxhjsgub05prJlAXiBORTSLy5zJaq1KqhGqF1idi3HfENXyYyJNfc2BSdw7tS7U7lu0k942id3M6nSYpKcnuGEpVCJuWz6X5ujHk4OCnHq9x7Y0D7I7kNiKSbIxxFrZffxJJKXVFOvS+j1/uX85JRwhtVw0jbvaz5GRn2x3LFlqgSqkr1qhFW+qNWcuGoJuI2T+TrZP6kXmi4l1OqAWqlCqRKtVq0HnUAhJa/5HW55I4+/r1pG1eZ3escqUFqpQqMXE4iLr3Wfbc9gn+5hKNPosl8fPX7Y5VbrRAlVKl1srZC7/H17A7MILIzX8i4fUHybpwzu5YbqcFqpQqE8F1G9Fy3HfE1X+QqJ8XsX9SD478tMvuWG6lBaqUKjP+lQKI+f3rbIh5gwaXDhD43o1sXb3I7lhuowWqlCpznfo8wMn7l5HpqEXEiiHE/+uPPnmpkxaoUsotGoe3p87otWy8qifR+6azZfItZJ48bnesMqUFqpRym6rVg+j8zELiW/6BNmcTOT3tevb8mGB3rDKjBaqUcitxOIge9Dy7+88j0GRR/5NbSVr8pt2xyoQWqFKqXLSKuhl5bDV7Alvi3DCehDce5mLWBbtjlYoWqFKq3ITUa0LLcSuJrzeYqOOfsXdiD46m77Y7VolpgSqlypV/pQCiH5vBhqipNLq0j0rv3MCPa/P/imHvoAWqlLJFp34Pc/y+ZZx2BNF6+YPEz3kBk+Ndv0NdC1QpZZumLTsQPGoNm2t0J3rPNDZNvo3TmSfsjuUyLVCllK2qX1WLjqO/ID58DO3O/JtTU69n33bv+AXpWqBKKduJw0H04D+zs+9cqphz1JnXn6SvZtkdq1haoEopjxER0w+Gr2Z/QAucSeOInz7Moy910gJVSnmUkAZNaTHue+Lr3Et0xifsnnQjGYf22R2rQFqgSimPUykgkOgnZpHcZTJNL+5GZvUg5d9L7I71G1qgSimP1fmWYRwbuIRzjmq0XDaY+A9f8qhLnbRAlVIerVlrJ7WeXsuW6tcTnTaFja/GcuaXk3bHArRAlVJeoEZQbTqOWUz81U/T/vQafp7alf07NtgdSwtUKeUdxOEg+oGX2X7zB1TPOU3Ix/1IXjLb1kxaoEopr9L2+tvIfvQHDlQKo3PiKOLffIxLF7NsyaIFqpTyOnUahtF83CoSQu4m+ujH7JrYk+NHfir3HFqgSimvFBBYmaiR75HUaQJhF3diZnZne8Kycs2gBaqU8mrO2x/jyO++Iksq02LJIOLn/qXcLnXSAlVKeb2wNlFc9fQ6fqwWRfTOiWyYchdnT59y++tqgSqlfMJVNYNpP+Yr4sJG0OGXVWRM6cpPOze59TVdKlAR6SsiqSKSJiLjC9gfKCLzrf0JItIsz77nrO2pItKnuDlFJMyaY5c1Z0DplqiUqigcfn7EDPkb2296n6CcTGp/1JeNy9533+sVN0BE/IDpQD8gAhgkIhH5hg0FThpjWgBTgAnWsRHAQKAN0BeYISJ+xcw5AZhijAkHTlpzK6WUy9p2iyVr6PccqtSYjo3MJUQAAAXHSURBVHFPET/zCS5fuljmr+PKO9BIIM0Ys8cYcxGYB8TmGxML/KfmFwK9RESs7fOMMVnGmL1AmjVfgXNax/S05sCa846SL08pVVHVa9yCpmN/ICH4DqKPfETqxF6cP3u6TF/DlQJtCBzI8zzd2lbgGGPMZSATCC7i2MK2BwOnrDkKey0ARGS4iCSJSFJGRoYLy1BKVTSBlasS9eT7rO/wN85Wb0rlKtXKdH5/F8ZIAduMi2MK215QcRc1/rcbjZkFzAJwOp0FjlFKKYAud4wARpT5vK68A00HGud53gg4VNgYEfEHgoATRRxb2PbjQE1rjsJeSymlPIIrBboeCLfOjgeQe1Io/02cFwNDrMcDgJXGGGNtH2idpQ8DwoHEwua0jvnemgNrzkUlX55SSrlPsd/CG2Mui8hIYBngB7xnjEkRkZeBJGPMYuBd4AMRSSP3nedA69gUEVkAbAMuAyOMMdkABc1pveSzwDwR+Quw0ZpbKaU8juS+6fNuTqfTJCV5x21QlVLeQ0SSjTHOwvbrTyIppVQJaYEqpVQJaYEqpVQJaYEqpVQJ+cRJJBHJAPZf4WEh5F536u18ZR2ga/FEvrIOKNlamhpjQgvb6RMFWhIiklTU2TVv4SvrAF2LJ/KVdYB71qLfwiulVAlpgSqlVAlV5AKdZXeAMuIr6wBdiyfylXWAG9ZSYT8DVUqp0qrI70CVUqpUtECVUqqEfKpARaS2iCy3bki3XERqFTJuiDVml4gMybO9s4hstW50N826xQgi8pKIHBSRTdaf/t66ljz7x4qIEZEQb12LiLwiIlusr8m3ItLAS9cxUUR2WGv5XERqunMdbl7LPSKSIiI5IuLWy5+kHG92WShjjM/8Af4JjLcejwcmFDCmNrDH+ruW9biWtS8RiCH3N+N/A/Sztr8EjPWFtVj7GpP7qwT3AyHeuhbgqjzHPwXM9NJ13Az4W48nFDSvF62lNdASWAU43ZjfD9gNNAcCgM1ARL4xT/zn3wS5v2JzvvU4whofCIRZ8/i5Mmf+Pz71DpRf39yusBvS9QGWG2NOGGNOAsuBviJSn9z/IeNM7n/lOYUcX17cuZYpwB8o5HYpbuCWtRhjfslzfDXcvx53reNb87/7gMWTeycGd3PXWrYbY1LdH7/8bnZZVAhfK9C6xpjDANbfdQoYU9SN7tIL2P4fI61vsd4r7NudMuaWtYjI7cBBY8xmd4QuhNu+LiLyVxE5AAwG/lzGufNz57+v/3iE3Hd07lYea3Gn8rzZZaFcuamcRxGR74B6Bex63tUpCthW3A3t3gResZ6/Akwm9x96qZT3WkSkqjX3zS7O7zKbvi4YY54HnheR54CRwIsuvl7BIWxah/Xaz5N754aPXHytooPYuJZyUJ43uyyU1xWoMeamwvaJyFERqW+MOWx9m3GsgGHpwA15njci9/OadH79rdN/b2hnjDma5zXeBr4qaf68bFjL1eR+5rPZ+sy/EbBBRCKNMUdKsRRbvi75zAW+ppQFatc6rBM0twK9rG+LS80DvibudCU3u0wX1252iQtz/pq7P6wuzz/ARH79wfg/CxhTG9hL7ofitazHta1964Fo/vfBeH9re/08xz9D7ucnXrmWfMfvo3xOIrnr6xKe5/gngYVeuo6+5N43LNTdX4vy+veF+08i+ZN7UiuM/53waZNvzAh+fRJpgfW4Db8+ibSH3BNIxc75mxzl9QUrp38UwcAKYJf193++2E7gnTzjHiH3g+M04OE8253Aj+SeiXuD//2k1gfAVmALuXcare+ta8n3GvsonwJ119flU2v7FuBLoKGXriON3M/eNll/3Ho1gZvXcie57/CygKPAMjeuoT+w08rwvLXtZeB263Fl4BMreyLQPM+xz1vHpfLrK1R+M2dRf/RHOZVSqoR87Sy8UkqVGy1QpZQqIS1QpZQqIS1QpZQqIS1QpZQqIS1QpZQqIS1QpZQqof8HcuFNQRRJJe0AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 360x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "f, ax = plt.subplots(1, figsize=(5,4))\n",
    "ax.plot(U[:,0], U[:,(1,2)]);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
