

\documentclass[main.tex]{subfiles}

\begin{document}


\chapter{Introduction}

\section{What is this course about?}

\mnote{Composite} 
The aim of the course is to provide a unified view to the 
behavior of materials and structures made of composites that combine 
brittle matrix with tensile reinforcement.
The term "composite" denotes a material made up of constituent materials that play different roles in the material behavior.
In cementitious composites the constituents are specialized on compressive and tensile behavior.
The concrete matrix material with high compressive strength surrounds and supports the relative position of the reinforcement material (steel, carbon or glass) with high tensile strength.

The term "brittle" implies a 
\mnote{Brittle matrix}
sudden local failure that is observed as 
a distinguished fracture of material. In the usual sense, this property is associated 
with glass, ceramics and carbon. Such materials exhibit perfectly 
elastic deformation and sudden failure.

\mnote{Behavior of concrete} 
To large extent, concrete and cementitious matrices can also be regarded
as brittle. Upon tensile loading macroscopically observable cracks develop 
that initiate a sudden failure. However, in comparison to completely brittle materials
mentioned above, the failure is a little bit less sudden. As the material structure 
of concrete consists of a skeleten of 
aggregates interacting via the cement paste, the process of crack localization and propagation
is much more complex.
The evolution of a macroscopically observable crack involves a coalescence of  microcracks.
This process is governed by local dissipative mechanisms of internal friction
and sliding between aggregates at the scale of the material structure.
Such kind of gradual material disintegration is specific to concrete.
Thus, concrete cracking is not necessarily a sudden event but a gradual process 
involving the development of microcracks and their localization to a macrocrack.
Phenomenologically, this process leads to nonlinear stress-strain behavior. In fact, concrete is neither elastic, nor plastic and even not purely brittle.
It is something in-between so that its material behavior is  referred to as "quasi-brittle". 
The complexity of the crack localization process makes 
an objective general description of concrete material behavior a challenging task and remains a subject of an ongoing research.

\mnote{Behavior of reinforced concrete}
Simply speaking, one could say, that the only reason for putting steel into concrete
is to prevent the failure upon the appearance of a crack. But this view would undervalue
the role of concrete within the composite. One could also say, that in combination 
with the reinforcement the cracking becomes a positive feature of the material behavior.
Without cracks, the reinforcement could not really exploit its strength. This symbiosis 
results in a large deformation capacity at a slowly increasing loading providing the desired 
ductile behavior. More precisely, we speak about "quasi-ductility" because the large deformation 
is owing to interacting cracking and debonding to distinguish
it from the ductility achieved by plastic yielding of material.

\mnote{Relation to practice}
Compared to materials with a more regular and uniform material 
skeleton, a realistic and generally valid description of concrete material behavior 
remains a challenging task and subject of ongoing research. 
Only a few finite-element tools are available on a commercial market that 
provide a support for realistic simulation of concrete material behavior.
Due to inherent limitations of the standard finite element solvers, these
tools must be used with care and knowledge of the specifics of 
the quasi-brittle material behavior. 

\mnote{Tools to look inside}
To provide a focused view at elementary
mechanisms of material disintegration, a  
set of software applications has been developed 
to support the course. It contains both analytical, closed form
models and finite-element models of debonding and crack propagation.
The BMCS-Tool Suite is available online. It is implemented within the 
general purpose scientific-computing environment based on the Python language.
Students can seamlessly start to learn the rich and powerful 
functionality of the tools that can be applied for fast prototyping
of models and dimensioning and assessment rules or for evaluation of experimental data.

\begin{figure}
\centering
  \includegraphics[width=0.9\textwidth]{fig/Lecture01/composite_types.png}
	\caption{Examples of considered brittle-matrix composites}
	\label{fig:composite_types}
\end{figure}


\mnote{Considered types of composites}
The explained concepts are applicable to the following types of composites exemplified in Fig.~\ref{fig:composite_types}
\begin{description}
\itemsep=-1mm
\item[A: Continuous, discrete reinforcement elements] which are explicitly placed by design at a given location. 
This represents the bar reinforcement in steel-reinforced concrete or reinforcement using CFRP or GFRP bars. 
The cross-sectional area of individual bars is of the order of centimeters.
\item[B: Dispersed continuous reinforcement] denoting fine meshes and grids of reinforcement, for example textile fabrics or mats. 
This reinforcement represents a layer acting in two dimensions. Profiled textiles and prefabricated three-dimensional structures 
are also included in this category. 
\item[C: Discontinuous, dispersed short fiber reinforcement] with randomly distributed and oriented fibers. This type of reinforcement is a part of the concrete mixture.
\end{description}

The behavior of the three types of composites is certainly different and should be 
considered complementary. Each of them is associated with different type of behavior 
and different manufacturing methods so that they are suitable for different 
types of applications in architecture and structural engineering.
The description of their behavior, even though quantitatively different, 
can be boiled down to the same key mechanism, the interacting matrix cracking 
and debonding of matrix and reinforcement.

\mnote{Material design}
The detailed knowledge of these mechanisms is a paramount for a reliable and econonic design of brittle-matrix composites and their applications. By harmonized use of different types of concrete mixtures, fiber cocktails, textile reinforcement and discrete bar reinforcement the design space for new specialized materials with axactly tuned properties becomes huge. The level of initial stiffness, the ductility, the ultimate tensile strength can be modified as desired.

\mnote{Research method}
Besides the topics related to the material behavior and design, the course will present the concepts of scientific practice for material research explaining the disciplines ofexperimental design, model calibration, model validation, parametric study. 

\section{Scope and structure of the course}

\mnote{Three structural levels}
This course aims to present this knowledge by three 
cases of macroscopic, structural behavior.
In the first part of the course, we look into the debonding process 
between two material components on an example of a pullout test. 
In the second part,
we will describe the crack propagation in concrete by considering
a bending test and its modifications. In the third part,
we describe the interaction of cracking and debonding using 
tensile, and bending test of a composite specimen.

\mnote{Three perspectives}
In each of these three parts we will use 
these three perspectives of observation. 
The subjective, special, unique orconcrete  perspective will be used
to discuss the experimental observation
of the structural response, i.e. the test results.
In the second perspective, a ready made model of the observed
structural behavior will be presented and will be used 
to "play around" with the experiment asking the question:
\begin{quote}
\textit{What happens when something gets changed?}
\end{quote}
The model will be considered as virtual experiment, as a kind 
of black-box tool. In the third, most objective perspective, 
will then regard the structural response as an interaction  
of local behavior of the material.

\mnote{Putting it together}
By following this scheme, we will develop a general and in-depth understanding 
of material and structural behavior applicable to wide range of
materials. Except of traditional, steel-reinforced concrete
we will apply it to innovative composite materials using 
nonmetallic reinforcement materials, like carbon bars.
In the last part of the course, we will sketch a design of a structure 
that exploits the material quasi-ductile behavior of cementitious composite 
in a safe and economic way. We will discuss the aspects of the structural behavior, 
in view of stress-redistribution and ductility both at the level of a cross 
section and at the level of the whole structure. Then, we will compare 
this view with the linear-elastic analysis and prediction of ultimate failure. 
This discussion will provide the basis for understanding of 
structural redundancy and safety assessment.



\section{Example applications of cementitious composites}

\mnote{Application domain}

The application domain of brittle-matrix composites
is still rapidly growing. Except of the traditional domain of steel-reinforced concrete (reinforcement of type A), innovative composites applying non-metallic reinforcement are included. Lightweight structural elements, like thin-shells provide appealing features, like low material consumption and high durability. 

Many examples of structures applying also non-metallic reinforcement already exist and a number of them has been realized in the framework
of research projects at the Institute of Structural Concrete of the RWTH Aachen University. On the one hand, thin walled 
shells made of textile-reinforced concrete \ref{fig:shells} applying the reinforcement layout of type B 
demonstrate the feasibility and the specific features of the new composite materials. On the other hand 
an application of CFRP as bar reinforcement (type A) has realized in collaboration is shown in Fig.~\ref{fig:albstadt}.

\mnote{Design rules for new composites}
However, their broader applicability is hindered by a missing support for engineering design rules. 
The traditional design codes cannot be simply adopted, because of qualitative differences in their material behavior. Application of these new types of composites requires the development of new codes and assessment methods from scratch. Standardization of testing methods is also an important issue.


\begin{figure}[tb]
\centering
\begin{subfigure}{0.45\linewidth}
	\centering
  \includegraphics[height=5cm]{fig/Lecture01/vault_shell.png}
	\caption{Campus Melaten, Aachen}
	\label{fig:vault_shell}
\end{subfigure}
\hspace{0.5cm}%
\begin{subfigure}{0.45\linewidth}
	\centering
  \includegraphics[height=5cm]{fig/Lecture01/t3.png}
	\caption{T3 Pavillon, RWTH Aachen}
	\label{fig:t3_pavillon}
\end{subfigure}
\caption{Examples of textile-reinforced concrete shells}
\label{fig:shells}
\end{figure}
\begin{figure}
\centering
  \includegraphics[height=5cm]{fig/Lecture01/trc_alpstadt.png}
	\caption{TRC bridge, Albstadt}
	\label{fig:albstadt}
\end{figure}

Except of the mentioned cases, the methods described in this course are also relevant for the analysis of FRP sheet lap joints applied for retrofitting of aging structures. At the same time, they are relevant also for application of TRC as a  strengthening layer of existing structures, such as bridges.

\section{BMCS Tool Suite}

\mnote{Interactive demonstration of the taught concepts}
The BMCS Tool Suite is a package of specialized programs that can be used by the course participants to study particular effects of the material- and structural behavior. The idea is to show the students an implemented model of a particular test setup in an interactive form first so that they can perform some virtual experiments. After showing a few examples of what kind of behavior the model can describe, the theory behind the model can be explained with a clear specification about the purpose and assumptions related to the model. 

\mnote{Open-source bundle for scientific computing}
The BMCS Tool Suite has evolved during several years of research in the field of material modeling of brittle-matrix composites. This development has been carried out using open-source scientific computing libraries developed in Python language. The computing environment consists of several hundreds of open-source, free libraries. For convenience, they are packaged within the Anaconda software bundle, which is free for academic purposes. The Installation of Anaconda can easily be performed on all existing platforms including, Windows, Mac and Linux operating systems. 

\mnote{Role of the BMCS models in the course}
The models implemented in BMCS-Tool can be installed on any computer platform. The implementations are computationally efficient, they are equipped with a simple interactive user interfaces that allows the students to start with a study of a problem at hand from scratch without any prerequisite knowledge. At the same time, students have a chance to learn modern implementation concepts of models during the course using a high-level programming language with a large expressive power. Just to mention an example, this environment enables us to implement a finite-element solver of a pull-out problem on some 15 lines of code, which is very closely related to the mathematical description of the model. These features make this environment ideally suited for the teaching concept followed within the BMCS course. 

The installation guide to the \lstin{Anaconda} bundle and \lstin{BMCS} is provided at the moodle page of the course.
{here} 

\end{document}


