{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example 3.1: Piecewise linear bond slip law \n",
    "This sheet demonstrates the implemented finite element model that shall be used later on to study the debonding behavior of different materials. You can modify the parameters of the bond slip law to study its effect on the structural behavior."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "from bmcs.api import PullOutModel\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define the material parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w_max = 0.1  # [mm]\n",
    "d = 16.0  # [mm]\n",
    "E_f = 210000  # [MPa]\n",
    "E_m = 28000  # [MPa]\n",
    "A_f = (d / 2.)**2 * np.pi  # [mm^2]\n",
    "P_b = d * np.pi\n",
    "A_m = 10 * d * 10 * d  # [mm^2]\n",
    "L_x = 20 * d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Construct the finite element model of the Pullout\n",
    "The class PullOutModel can be configured using different types of material models, number of elements and loading scenarios. In the present case, lets use it in a simple way applying a linear kind of bind-slip law up to the pullout displacment $w=0.1\\;\\mathrm{mm}$ "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pm = PullOutModel(mats_eval_type='multilinear',\n",
    "                  n_e_x=50, w_max=w_max)\n",
    "pm.tline.step=0.02\n",
    "pm.tloop.verbose=False\n",
    "pm.loading_scenario.loading_type = 'monotonic'\n",
    "pm.mats_eval.trait_set(E_m=E_m, E_f=E_f,\n",
    "                       s_data='0, 0.01, 0.1',\n",
    "                       tau_data='0, 1, 0.1')\n",
    "pm.mats_eval.update_bs_law = True\n",
    "pm.cross_section.trait_set(A_m=A_m, P_b=P_b, A_f=A_f)\n",
    "pm.geometry.L_x = L_x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bond-slip law\n",
    "The material model of the pullout model is accessible via the attribute mats_eval. Let us check if the bond slip law has been configured correctly by plotting it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pm.mats_eval.bs_law.plot(plt, color='green')\n",
    "plt.xlabel('slip [mm]')\n",
    "plt.ylabel('tau [MPa]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run the solver\n",
    "The model will use the defined discretization, assemble the stiffness matrix of the system and solve it by returning the diplacement field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pm.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Postprocessing\n",
    "Let us now access the fields obtained by the solver"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pullout response"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "P, w0, wL = pm.get_Pw_t()\n",
    "plt.plot(wL,P)\n",
    "plt.plot(w0,P)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Displacement fields"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pm.plot_u_p(plt.axes(),2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Slip field"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pm.plot_s(plt.axes(),1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Shear flow $T = \\tau p$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pm.plot_sf(plt.axes(),0.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Strain fields"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pm.plot_eps_p(plt.axes(),0.9999)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Force fields $F = \\sigma_c A_c$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pm.plot_sig_p(plt.axes(),0.02)\n",
    "pm.plot_sig_p(plt.axes(),0.999)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Questions and tasks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1 How many elements are required to get the accaptable accuracy?\n",
    "Test if the solution changes if the number of elements gets changed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2 Why are the stresses and strains constant within a finite element?\n",
    "If displacements are linear within an element - the strains are their derivatives ... "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3 At which distance from the loaded point do we get zero slip?\n",
    "Recalling that we assumed $a$ finite for the examples with constant bond slip law. Can we find such a distance in case of the linear bond-slip law?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
